open Td;;

let in_bounds (row, col) =
  row >= 0
  && col >= 0
  && row < Const.height
  && col < Const.width
;;

let cardinal_offset =
  [
    (0, 1);
    (1, 0);
    (0, -1);
    (-1, 0);
  ]
;;

let move_offset = function
  | `Horizontal -> (0, 1)
  | `Vertical -> (1, 0)
  | `Empty -> (0, 0)
;;

let all_empty grid =
  let result = ref [] in
    Array.iteri (fun ir row -> Array.iteri (fun ic t ->
      if (t = `Empty) then
        result := (ir, ic) :: !result
    ) row) grid;
  !result
;;

let empty grid (row, col) = grid.(row).(col) = `Empty;;

let rec has_neighbor grid ((par, pac), (pbr, pbc)) = function
  | [] -> false
  | (off_row, off_col) :: tail ->
      let tar, tac = par + off_row, pac + off_col in
      let tbr, tbc = pbr + off_row, pbc + off_col in
        if (in_bounds (tar, tac) && not (empty grid (tar, tac)))
        || (in_bounds (tbr, tbc) && not (empty grid (tbr, tbc))) then true
        else has_neighbor grid ((par, pac), (pbr, pbc)) tail
;;

(* second_piece returns the location of the second part of a tile, and assumes that (row, col) is in bounds and empty *)
let second_piece state player (row, col) =
  let off_row, off_col = move_offset player in
  let tr, tc = row + off_row, col + off_col in
    if in_bounds (tr, tc) && (state.grid.(tr).(tc) = `Empty) then 
      Some (tr, tc)
    else None
;;

let valid_moves state player =
  let m = all_empty state.grid in
    List.filter (fun move -> 
      match second_piece state player move with
      | None -> false
      | Some loc2 -> true (* (has_neighbor state.grid (move, loc2) cardinal_offset) *)
    ) m
;;

