type tile = [ `Horizontal | `Vertical | `Empty ];;

(*type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;*)

class timer limit =
  object(self)
    val mutable go_time = 0.0
    val limit = limit
    method set_go_time = go_time <- Sys.time ()
    method time_remaining =
      1000. *. ((limit /. 1000.) -. ((Sys.time ()) -. go_time))
  end
;;

type state =
 {
  grid : tile array array;
  mutable row_count : int; (* used to track grid lines during input *)
  mutable my_id : tile;
  timer : timer;
 }
;;

